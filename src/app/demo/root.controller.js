(function () {
    'use strict';

    angular
        .module('app')
        .controller('rootController', rootController);

    rootController.$inject = ['$scope'];
    function  rootController($scope) {
        $scope.globalValue = 'This is abstract root page value';
    }
})();