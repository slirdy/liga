(function () {
    'use strict';

    angular
        .module('app')
        .controller('factoryController', factoryController);

    factoryController.$inject = ['myFactory'];
    function factoryController(myFactory) {

        console.log(myFactory.myCos(10));
    }
})();