(function () {
    'use strict';

    angular
        .module('app')
        .controller('aboutController', aboutController);

    aboutController.$inject = ['$scope', 'resData'];
    function aboutController($scope, resData) {
        
        $scope.about = 'nested state';
        
        $scope.aboutClick = function () {
            $scope.about = $scope.about + '.';
        };
        
        $scope.getName = function () {
            return resData.data.name;
        };
    }
})();