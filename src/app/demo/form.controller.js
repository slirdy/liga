(function () {
    'use strict';

    angular
        .module('app')
        .controller('formController', formController);

    formController.$inject = ['$scope'];
    function formController($scope) {
        $scope.name = '';
        $scope.formSubmit = function () {
            console.log("Name: " + $scope.name);
        };
    }
})();