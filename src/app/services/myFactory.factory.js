(function () {
    'use strict';

    angular
        .module('app')
        .factory('myFactory', myFactory);

    myFactory.$inject = [];
    function myFactory() {
        function myCosinus(x) {
            return 'Factory result = ' + x;
        }

        return {
            myCos: myCosinus
        };
    }
})();