(function () {
    'use strict';

    angular
        .module('app')
        .service('dataService', dataService);

    dataService.$inject = ['$http', '$q', 'api_url'];
    function  dataService($http, $q, api_url) {
        var self = this;

        self.data = {
            name: 'Loading...'
        };

        this.getData = function () {
            var defer = $q.defer();

            setTimeout(function () {
                $http.get(api_url + 'data.json').then(
                    function (response) {
//                        self.data.name = response.data.name;
                        defer.resolve(response);
                    },
                    function (response) {
                        console.log(response);
                    }
                );
            }, 1000);

            return defer.promise;
        };
    }
})();