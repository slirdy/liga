(function () {
    'use strict';

    angular
        .module('app')
        .constant('api_url', '/liga/src/app/data/')
        .config(appConfig);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    function appConfig($stateProvider, $urlRouterProvider) {

        $urlRouterProvider
            .when('', '/home')
            .when('/about', '/about/directive')
            .otherwise('/home');

        $stateProvider
            .state({
                name: 'root',
                abstract: true,
                url: '',
                templateUrl: 'app/layout/layout.html',
                controller: 'rootController'
            })
            .state({
                parent: 'root',
                name: 'home',
                url: '/home',
                views: {
                    'header': {
                        templateUrl: 'app/layout/header.html'
                    },
                    'content': {
                        templateUrl: 'app/demo/home.html'
                    }
                }
            })
            .state({
                parent: 'root',
                name: 'form',
                url: '/form',
                views: {
                    'header': {
                        templateUrl: 'app/layout/header.html'
                    },
                    'content': {
                        controller: 'formController',
                        templateUrl: 'app/demo/form.html'
                    }
                }
            })
            .state({
                parent: 'root',
                name: 'about',
                url: '/about',
                views: {
                    'header': {
                        templateUrl: 'app/layout/header.html'
                    },
                    'content': {
                        templateUrl: 'app/demo/about.html',
                        controller: 'aboutController'

                    }
                },
                resolve: {
                    resData: ['dataService', function (dataService) {
                            return dataService.getData();
                        }]
                }
            })
            .state({
                parent: 'about',
                name: 'directive',
                url: '/directive',
                template: '<adder></adder>'
            })
            .state({
                parent: 'about',
                name: 'component',
                url: '/component',
                template: '<mult></mult>'
            })
            .state({
                parent: 'about',
                name: 'service',
                url: '/service',
                template: '<span>Resolve result<br> data.name: {{ getName() }}</span>'
            })
            .state({
                parent: 'about',
                name: 'factory',
                url: '/factory',
                controller: 'factoryController',
                template: '<h3>Result in console</h3>'
            });
    }
})();