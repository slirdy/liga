(function () {
    'use strict';

    angular
        .module('app')
        .controller('multController', multController);

    multController.$inject = ['$scope'];
    function multController($scope) {
        $scope.n1 = 0;
        $scope.n2 = 0;
        $scope.result = function () {
           var res = +$scope.n1 * +$scope.n2;
           if (isNaN(res)){
               res = 'Wrong arguments';
           }
           return res;
        };
    }
})();