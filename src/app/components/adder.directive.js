(function () {
    'use strict';

    angular
        .module('app')
        .directive('adder', adderDirective);

    adderDirective.$inject = [];
    function adderDirective() {

        return {
            templateUrl: 'app/components/adder.html',
            controller: 'adderController'
        };
    }
})();