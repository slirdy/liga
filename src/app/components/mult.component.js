(function () {
    'use strict';

    angular
        .module('app')
        .component('mult', {
            templateUrl: 'app/components/mult.html',
            controller: 'multController'
        });
    
})();