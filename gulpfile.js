var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require("gulp-rename"),
    htmlmin = require('gulp-htmlmin'),
    clean = require('gulp-clean');

gulp.task('js_concat', function () {
    return gulp.src(['src/app/app.module.js', 'src/**/*.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('build/js'));
});

gulp.task('js_uglify', ['js_concat'], function () {
    return gulp.src('build/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('build'));
});

gulp.task('html_all', function () {
    return gulp.src('src/app/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('build/app'));
});

gulp.task('html', function () {
    return gulp.src('src/index_dist.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(rename('index.html'))
    .pipe(gulp.dest('build'));
});

gulp.task('style', function () {
    return gulp.src('src/app/**/*.css')
        .pipe(gulp.dest('build/app'));
});

gulp.task('clean', function () {
    return gulp.src('build', {read: false})
        .pipe(clean());
});

gulp.task('data', function () {
    return gulp.src('src/app/**/*.json')
        .pipe(gulp.dest('build/app'));
});

gulp.task('build', ['js_concat', 'js_uglify', 'html', 'html_all', 'data', 'style'], function () {});

gulp.task('default', ['build'], function () {});